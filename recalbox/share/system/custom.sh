#!/bin/sh
# custom.sh
recallog "Custom.sh called"
case "$1" in
    start)
        recallog "Start detected."
        # debug
        /usr/bin/python /recalbox/share/system/lcd_or_tv/script.py 2>&1 | recallog &
        ;;
    stop)
        recallog "Stop detected."
        ;;
    restart|reload)
        recallog "Restart|Reload detected"
        ;;
    *)
        recallog "Other state detected..."
esac
