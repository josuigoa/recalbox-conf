import os
import RPi.GPIO as GPIO
import time
import datetime
import subprocess
import re

# /recalbox/share/system/script.py

def subproc_output(cmd):
    try:
        o = subprocess.check_output(cmd, shell=True)
        return o
    except subprocess.CalledProcessError as e:
        info = 'ERROREA: cmd: ' + str(e.cmd) + ' (' + str(e.returncode) + ') kodea / output: ' + str(e.output)
        print info
        return info
    
def shutdown():
  os.system('shutdown -h now')

def reboot():
  os.system('shutdown -r now')

def get_current_output():
    file_object = open('/recalbox/share/system/lcd_or_tv/curr_output.txt', 'r')
    try:
        curr_output = file_object.readline().rstrip('\n')
        hdmi_mode = int(file_object.readline())
        w = int(file_object.readline())
        h = int(file_object.readline())
    except:
        curr_output = ''
        hdmi_mode = -1
        w = 0
        h = 0
    file_object.close()
    return curr_output, hdmi_mode, w, h

def set_output_to(next_output, next_hdmi_mode, next_w, next_h):
    print 'set_output_to: ' + next_output
    # os.system('mount -o remount,rw /boot')

    file_object = open('/recalbox/share/system/lcd_or_tv/curr_output.txt', 'w')
    file_object.write(str(next_output) + '\n')
    file_object.write(str(next_hdmi_mode) + '\n')
    file_object.write(str(next_w) + '\n')
    file_object.write(str(next_h) + '\n')
    config_txt_berria = open('/recalbox/share/system/lcd_or_tv/config.' + next_output + '.txt', 'r').read()
    recalbox_conf_berria = open('/recalbox/share/system/lcd_or_tv/recalbox.' + next_output + '.conf', 'r').read()
    # n64_emu_conf_berria = open('/recalbox/share/system/lcd_or_tv/mupen64plus.' + next_output + '.cfg', 'r').read()

    file_object.close()
    f = open('/boot/config.txt', 'w')
    f.write(config_txt_berria)
    f.close()
    f = open('/recalbox/share/system/recalbox.conf', 'w')
    f.write(recalbox_conf_berria)
    f.close()
    # f = open('/recalbox/share/system/configs/mupen64/mupen64plus.cfg', 'w')
    # f.write(n64_emu_conf_berria)
    # f.close()
    reboot()

def extract_mode_res(line):
    mode = 87
    res = '800x480'
    w = 800
    h = 480

    res_re = re.compile('([0-9]{3,4})x([0-9]{3,4})')
    match = res_re.search(line)
    if match:
        res = match.group(0)
        w = int(match.group(1))
        h = int(match.group(2))

    mode_re = re.compile('mode ([0-9]{1,2}):')
    match = mode_re.search(line)
    if match:
        mode = int(match.group(1))
    
    return mode, w, h
    

def get_tvservice_mode(file_tvserv):
    cea_out = subproc_output('tvservice --modes=CEA')
    file_tvserv.write('------------ tvservice --modes=CEA -----------\n')
    file_tvserv.write(cea_out)
    dmt_out = subproc_output('tvservice --modes=DMT')
    file_tvserv.write('------------ tvservice --modes=DMT -----------\n')
    file_tvserv.write(dmt_out)

    out = cea_out + dmt_out
    output = ''
    hdmi_drive = -1
    hdmi_group = 0
    hdmi_mode = 0
    w = 0
    h = 0
    
    out_lines = out.split('\n')
    index_native = -1
    index_87 = -1
    for idx, out_line in enumerate(out_lines):
        if out_line.find('(native)') != -1:
            index_native = idx
        if out_line.find('mode 87') != -1:
            index_87 = idx
    
    if index_native != -1:
        hdmi_mode, w, h = extract_mode_res(out_lines[index_native])
        if index_native <= len(cea_out.split('\n')): # TV
            hdmi_drive = 2
            hdmi_group = 1
            output = 'tv'
    elif index_87 != -1:
        hdmi_mode, w, h = extract_mode_res(out_lines[index_87])
        # if (w == 800 and h == 480): # LCD
        output = 'lcd'
        # else: # ordegailuko pantaila
        #     next_output = 'tv'
    else:
        hdmi_mode, w, h = extract_mode_res(out_lines.pop())
    
    if hdmi_drive == -1:
        hdmi_drive = 1
        hdmi_group = 2
        output = 'lcd'
    
    return out, output, hdmi_drive, hdmi_group, hdmi_mode, w, h
    
def check_tvservice():
    print 'check_tvservice'
    file_tvserv = open('/recalbox/share/system/logs/tvserv_output.log', 'r')
    file_tvserv_content = file_tvserv.read()
    file_tvserv.close()

    file_tvserv = open('/recalbox/share/system/logs/tvserv_output.log', 'w')
    file_tvserv.write(file_tvserv_content)

    mode_out, next_output, hdmi_drive, hdmi_group, hdmi_mode, w, h = get_tvservice_mode(file_tvserv)
    
    file_tvserv.close()
    curr_output, curr_hdmi_mode, curr_w, curr_h = get_current_output()
    print 'CURRENT: output, hdmi_mode, w, h - ' + curr_output + ' ' + str(curr_hdmi_mode) + ' ' + str(curr_w) + ' ' + str(curr_h)
    print 'NEXT: output, hdmi_mode, w, h - ' + next_output + ' ' + str(hdmi_mode) + ' ' + str(w) + ' ' + str(h)
    print 'berdinak output: ' + str(next_output == curr_output)
    print 'berdinak hdmi_mode: ' + str(hdmi_mode == curr_hdmi_mode)
    print 'berdinak w: ' + str(w == curr_w)
    print 'berdinak h: ' + str(h == curr_h)
    if (next_output == curr_output and hdmi_mode == curr_hdmi_mode and w == curr_w and h == curr_h):
        return
    
    os.system('mount -o remount,rw /boot')
    # edozein telebistarako prestatzeko kode zatia
    # oraingoz nirearekin bakarrik funtzionatzeko 
    # konfigurazio fitxategia erabiliko dut
    # if next_output == 'tv':
    #     config_tpl = open('/recalbox/share/system/lcd_or_tv/config.tv.tpl.txt', 'r')
    #     config_tpl_content = config_tpl.read()
    #     config_tpl.close()
    #     replacement = hdmi_mode
    #     if hdmi_mode == 87:
    #         print 'mode 87'
    #         replacement = 'hdmi_mode=1\n' hdmi_mode + '\nhdmi_cvt ' + w + ' ' + h + ' 60 6 0 0 0')
        
    #     config_tpl_content.replace('::hdmi_mode::', replacement)
    #     f = open('/recalbox/share/system/lcd_or_tv/config.tv.txt', 'w')
    #     f.write(config_tpl_content)
    #     f.close()
    
    set_output_to(next_output, hdmi_mode, w, h)

# def switch_output():
#     curr_output = get_current_output()
#     if curr_output == 'tv':
#         set_output_to('lcd')
#     else:
#         set_output_to('tv')

# def check_long_press():
#     GPIO.setmode(GPIO.BCM)
    
#     PIN = 21

    # GARRANTZITSUA!!
    # adibide honek funtziona dezan, PIN pina GND (lurra, 0V) motako pin batekin
    # lotu behar da, horregatik jarriko dugu defektuz LOW dela eta horregatik
    # eginen dugu GPIO.wait_for_edge(PIN, GPIO.FALLING). 3V-eko pin batekin
    # lotuko bagenu, aginduak komentatuak daudenak izan behar lukete

    # pull_up_down=GPIO.PUD_UP honen bidez,
    # defektuzko balioa HIGH (True, 3V) izanen da.
    # Beraz, botoia sakatzean, itzuliko digun balioa LOW (False, 0V) izanen da
    # GPIO.setup(PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # 3V pinarekin: GPIO.setup(PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)


    # while True:
        # wait_for_edge funtzioaren bidez, PIN pineko balio 0ra aldatu bitarte programa blokeatuko da.
        # modu honetan, CPUaren erabilera arbuiagarria izanen da
        # GPIO.wait_for_edge(PIN, GPIO.FALLING) # 3V pinarekin: GPIO.wait_for_edge(PIN, GPIO.RISING)

        # print "Pressed"
        # start = time.time()
        # time.sleep(0.2)

        # while GPIO.input(PIN) == GPIO.LOW: # 3V pinarekin: while GPIO.input(PIN) == GPIO.HIGH:
        #     time.sleep(0.01)
        
        # length = time.time() - start
        # print length

        # if length > 3:
        #     print "Long Press"
        #     GPIO.cleanup()
        #     switch_output()
        # else:
        #     print "Short Press"

check_tvservice()

# check_long_press()